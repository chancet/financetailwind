import React, {Component} from 'react';
import yahooFinance from 'yahoo-finance';
import { TrendingDown, TrendingUp } from 'react-feather';

class StockIndex extends Component {
    
    constructor(props) {
        super(props);
        this.state = {};
        this.getIndices = this.getIndices.bind(this);
    }

    componentWillMount(){
        console.log("Mounted " + this.props.ticker);
        this.getIndices();
        setInterval(this.getIndices(), 60000);
    }

    getIndices() {
        yahooFinance.quote(
            {
                symbol: this.props.ticker,
                modules: ['price', 'summaryDetail']
            },
            (err, quotes) => {
                if (err) {
                    console.log(err);
                } else {
                    this.setState({ price: quotes.price, summary: quotes.summaryDetail});
                }
            }
        )
    }
    
    render () {
        if (this.state.price && this.state.price['regularMarketChange'] > 0) {
            let marketPrice = this.state.price['regularMarketChange'];
            return (
                <div className="px-4 text-center">
                    <div style={{ marginLeft: '40%'}}><TrendingUp size="15" color="#48BB78"/> </div>
                    <div><span className="text-green-500 text-bold text-xs">{this.state.price['regularMarketPrice'] ? marketPrice.toFixed(2) : null}</span></div>
                    <div className="text-xs">{this.state.price['shortName']}</div> 
               </div>
            );
        } else if (this.state.price) {
            return (
                <div className="px-4 text-center">
                    <div style={{ marginLeft: '40%'}}><TrendingDown size="15" color="#F56565"/></div>
                    <div><span className="text-red-500 text-xs">{this.state.price['regularMarketPrice'] ? this.state.price['regularMarketPrice'].toFixed(2) : null}</span></div>
                    <div className="text-xs">{this.state.price['shortName']}</div> 
               </div>
            );
        } else {
            return (<div>??</div>);
        }
    }
}

export default StockIndex;