import React, {Component} from 'react';
import yahooFinance from 'yahoo-finance';
import Loader from 'react-loader-spinner'

class TickerCard extends Component {
    
    constructor(props) {
        super(props);
        this.state = {};

        this.getQuotes = this.getQuotes.bind(this);
    }

    componentWillMount(){
        console.log("Mounted " + this.props.ticker);
        this.getQuotes();
        setInterval(this.getQuotes(), 60000);
    }

    getQuotes() {
        yahooFinance.quote(
            {
                symbol: this.props.ticker,
                modules: ['price']
            },
            (err, quotes) => {
                if (err) {
                    console.log(err);
                } else {
                    this.setState({ price: quotes.price });
                }
            }
        )
    }
    
    render () {
        if (this.state.price && this.state.price['regularMarketChange'] > 0) {
            return (
                <div className="sm:w-1/5 p-4 bg-white rounded-sm mx-2 border-t-4 border-green-500 shadow hover:shadow-lg cursor-pointer">
                    <h1 className="font-bold">{this.state.price['symbol']}</h1>
                    <h5 className="text-sm text-gray-500">{this.state.price['longName']}</h5>
                    <h4 className="pt-2 font-bold text-green-500"> {this.state.price['postMarketPrice']} (+{this.state.price['regularMarketChange']}%)</h4>
                </div>
    
            );
        } else if (this.state.price) {
            return (
                <div className="sm:w-1/5 p-4 bg-white rounded-sm mx-2 border-t-4 border-red-500 shadow hover:shadow-lg cursor-pointer">
                    <h1 className="font-bold">{this.state.price['symbol']}</h1>
                    <h5 className="text-sm text-gray-500">{this.state.price['longName']}</h5>
                    <h4 className="pt-2 font-bold text-red-500"> {this.state.price['postMarketPrice']} ({this.state.price['regularMarketChange']}%)</h4>
                </div>
    
            );
        } else {
            return (<div className="sm:w-1/5 p-4 bg-white rounded-sm mx-2 border-t-4 border-gray-500 shadow">
                        <Loader height={55} color="#A0AEC0" type="Grid" style={{ paddingTop: "10px", paddingBottom: "10px", marginLeft: "25%"}}/>
                    </div>);
        }
    }
}

export default TickerCard;