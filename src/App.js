import React, {Component} from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';

import TickerCard from './components/TickerCard';
import StockIndex from './components/StockIndex';

import './App.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tickers: ['FB', 'AMZN', 'AAPL', 'NFLX', 'GOOG'],
      indexes: ['^DJI', '^RUT', '^GSPC', '^IXIC']
    }
  }

  render(){
    return (
      <div className="App container-fluid mx-auto flex">
        <Grid>
          <Row className="border-b justify-between items-end">
            <Col xs={5}>
                <div className="text-3xl font-bold py-6">
                  {this.props.title}
                </div>
            </Col>
            <Col xs={7}>
                <div className="flex py-6 justify-end">
                  {
                    this.state.indexes.map( index  => {
                      return <StockIndex key={index} ticker={index} />
                    })
                  }
                </div>
            </Col>
          </Row>
          <Row>
            <Col className="mt-4">
              <h3 className="text-md font-bold">Stocks You Follow</h3>
            </Col>
          </Row>
          <Row className="border-b pb-4">
            <Col className="flex mt-4 mb-4 sm:w-full">
              
              {
                this.state.tickers.map( tickerSymbol => {
                  return <TickerCard key={tickerSymbol} ticker={tickerSymbol} />
                })
              }
  
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default App;
